<%--
  Created by IntelliJ IDEA.
  User: enrickenet
  Date: 01/03/2021
  Time: 13:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Panier</title>
    </head>
    <body>
        <%@include file="/WEB-INF/jsp/titre.jsp" %>
        <header>
            <h2>Votre panier ${ listePredefinie }</h2>
        </header>

        <div>
            <ul>
                <li>
                    <div>
                        <form method="get">
                            <a><input type="checkbox" name="coche"></a>
                            <a> test</a>
                        </form>
                    </div>
                </li>
            </ul>
        </div>


        <footer>
            <div>
                <a href="listes" title="retour listes">Retour</a>
            </div>
            <div>
                <a href="" title="reinitialiser panier">Réinitialiser le panier</a>
            </div>
        </footer>
    </body>
</html>
