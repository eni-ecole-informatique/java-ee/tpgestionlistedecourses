
<!--
    Fichier jsp qui permet d'inclure sur tous les jsp du projet
    'page' -> format encodage et langage utilisé
    'taglib' -> chargement de la bibliothèque JSTL
-->

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

