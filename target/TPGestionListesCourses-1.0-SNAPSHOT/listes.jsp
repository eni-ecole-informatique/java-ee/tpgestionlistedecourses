<%--
  Created by IntelliJ IDEA.
  User: enrickenet
  Date: 01/03/2021
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Listes prédéfinies</title>
    </head>
    <body>
    <%@include file="/WEB-INF/jsp/titre.jsp" %>

        <header>
            <h2> Listes prédéfinies</h2>
        </header>

        <div>
            <c:forEach var ="nomListe" items="${ listeAliments }">
                <ul>
                    <li>
                        <div>
                            <a> ${ nomListe } </a>
                            <a href="panier" title="Commencer les courses">Commencer</a>
                            <a href="" title="Supprimer">Supprimer</a>
                        </div>
                    </li>
                </ul>
            </c:forEach>

        </div>
    </body>
    <footer>
        <div>
            <a href="nouvelleListe" title="Ajouter une nouvelle liste">Ajouter une nouvelle liste</a>
        </div>
    </footer>
</html>
