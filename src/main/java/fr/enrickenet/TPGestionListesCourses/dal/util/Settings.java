package fr.enrickenet.TPGestionListesCourses.dal.util;

import java.io.IOException;
import java.util.Properties;

/**
 * Classe permettant le chargement des paramètres de connexion à la BDD par l'intermédiaire d'un fichier texte
 * avec extension .properties
 * @author Enrick Enet
 */
public class Settings
{
    private static Properties properties;

    //Chargement du fichier en mémoire avant l'appel à la méthode getProperty
    static {
        try {
            properties = new Properties();

            // 1 - Accès aux clés/valeurs du fichier texte 'connexion.properties'
            properties.load(Settings.class.getResourceAsStream("connexion.properties"));

        } catch (IOException e) {
            System.out.println("Cause : " + e.getCause());
        }
    }

    /**
     * Méthode qui permet d'obtenir une chaine de caractères correspondant à la clé renseignée en paramètre.
     * @param cle - un objet de type String.
     * @return - la valeur
     */
    public static String getProperty(String cle) {
        String valeur = properties.getProperty(cle);
        return valeur;
    }
}
