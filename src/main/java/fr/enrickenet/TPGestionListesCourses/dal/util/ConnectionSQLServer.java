package fr.enrickenet.TPGestionListesCourses.dal.util;

import com.microsoft.sqlserver.jdbc.SQLServerDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe qui gère la connexion à la BDD SQL Server
 */
public class ConnectionSQLServer
{
    //Déclaration des constantes qui vont servir à établir le lien avec les infos de connexion du fichier connexion.properties :
    private static String   CONST_URL,
                            CONST_USER,
                            CONST_PASSWORD,
                            CONST_URL_JDBC = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    public static Connection seConnecter() {
        Connection connexion = null;

        //Chargement du driver JDBC SQLServer
        try {
            Class.forName(CONST_URL_JDBC);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //Obtention de la connexion à SQLServer :
        try {
            DriverManager.registerDriver(new SQLServerDriver());
            //Récupération des propriétés de connexion à la BDD :
            CONST_URL = Settings.getProperty("URL");
            CONST_USER = Settings.getProperty("USER");
            CONST_PASSWORD = Settings.getProperty("PASSWORD");
            connexion = DriverManager.getConnection(CONST_URL,CONST_USER, CONST_PASSWORD );

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connexion;
    }
}
