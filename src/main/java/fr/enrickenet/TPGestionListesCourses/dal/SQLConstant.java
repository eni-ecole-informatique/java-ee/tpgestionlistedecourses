package fr.enrickenet.TPGestionListesCourses.dal;

/**
 * Classe qui centralise l'ensemble des constantes utilisées pour les requêtes SQL.
 */
public class SQLConstant
{
    protected static final String
            //Lister toutes les listes de courses
            SQL_SELECT_ALL = "SELECT id,nom FROM liste",

            //Sélectionner une liste par son identifiant
            SQL_SELECT_BY_ID = "SELECT id_liste, nom_liste, id_article, nom_article, coche "
                    + "FROM listes LEFT JOIN articles on id=id_liste WHERE id=?",

            //Ajouter une nouvelle liste
            SQL_INSERT_LISTE = "INSERT INTO listes(nom) values(?);",

            //Ajouter un nouvel article
            SQL_INSERT_ARTICLE = "insert into ARTICLES(nom, id_liste) values(?,?);",

            //Supprimer un article
            SQL_DELETE_ARTICLE = "delete from ARTICLES where id=?",

            //Supprimer une liste de courses
            SQL_DELETE_LISTE = "delete from LISTES where id=?",

            //Mettre à jour le statut (cocher) d'un article
            SQL_UPDATE_COCHE_ARTICLE="update ARTICLES set coche=1 where id=?",

            //Mettre à jour le statut (décocher) d'un article
            SQL_UPDATE_DECOCHE_ARTICLE="update ARTICLES set coche=0 where id=?",

            //Mettre à jour le statut (décocher) d'une liste de course
            SQL_UPDATE_DECOCHE_ARTICLES="update ARTICLES set coche=0 where id_liste=?";
}
