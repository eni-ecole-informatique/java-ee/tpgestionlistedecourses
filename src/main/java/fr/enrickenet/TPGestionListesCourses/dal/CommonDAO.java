package fr.enrickenet.TPGestionListesCourses.dal;

import fr.enrickenet.TPGestionListesCourses.bo.Article;
import fr.enrickenet.TPGestionListesCourses.dal.util.ConnectionSQLServer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Classe abstraite dont tous les DAO hériteront des méthodes.
 */
public abstract class CommonDAO<T>
{
    protected ConnectionSQLServer connexion = (ConnectionSQLServer) ConnectionSQLServer.seConnecter();

    /**
     * Méthode de création
     * @param object
     * @return boolean
     */
    public abstract T create(T object);


    /**
     * Méthode pour effacer
     * @param object
     * @return boolean
     */
    public abstract boolean delete(T object);

    /**
     * Méthode de mise à jour
     * @param object
     * @return boolean
     */
    public abstract boolean update(T object);


    /**
     * Récupérer l'objet à partir d'un champs de type entier
     * @param id
     * @return T
     */
    public abstract T findById(int id);


    /**
     * Permet de récupérer une liste d'objets via l'id.
     * @param id - un id.
     * @return
     */
    public abstract List<T> findAll(int id) throws SQLException;



}
