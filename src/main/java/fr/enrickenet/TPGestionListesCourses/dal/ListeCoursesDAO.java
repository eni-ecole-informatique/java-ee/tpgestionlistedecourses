package fr.enrickenet.TPGestionListesCourses.dal;

import fr.enrickenet.TPGestionListesCourses.bo.Article;
import fr.enrickenet.TPGestionListesCourses.bo.ListeCourse;
import fr.enrickenet.TPGestionListesCourses.dal.util.ConnectionSQLServer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui permet de gérer les différentes requêtes SQL avec la bas de données.
 * Sert d'interface d'accès au composant ListeCourses.
 * @author Enrick Enet
 */
public class ListeCoursesDAO extends CommonDAO<ListeCourse>
{
    List<ListeCourse> listeCourse = null;
    Article article = null;
    private Connection connexion = null;

    //SQL_INSERT_LISTE = "INSERT INTO listes(nom) values(?);",
    //SQL_INSERT_ARTICLE = "insert into ARTICLES(nom, id_liste) values(?,?);",
    @Override
    public ListeCourse create(ListeCourse listeCourse) {

        //Chargement de la BDD
        try (Connection connexion = ConnectionSQLServer.seConnecter()) {
            PreparedStatement preparedStatement = null;
            ResultSet resultat = null;

            if (listeCourse.getId() == 0) {
                preparedStatement = connexion.prepareStatement(SQLConstant.SQL_INSERT_LISTE,PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, listeCourse.getNom());
                preparedStatement.executeUpdate();

                //Récupération de la clé générée
                resultat = preparedStatement.getGeneratedKeys();

                if(resultat.next()) {
                    listeCourse.setId(resultat.getInt(1));
                }

                resultat.close();
                preparedStatement.close();
            }
            if (listeCourse.getArticles().size() == 1) {
                preparedStatement = connexion.prepareStatement(SQLConstant.SQL_INSERT_ARTICLE);
                //.GET(0).GETNOM() --> récupération du premier article de la liste de courses
                preparedStatement.setString(1, listeCourse.getArticles().get(0).getNom());
                preparedStatement.setInt(2,listeCourse.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connexion.commit();


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(ListeCourse object) {
        return false;
    }

    @Override
    public boolean update(ListeCourse object) {
        return false;
    }

    /**
     * Méthode qui se connecte à la BDD et qui permet de récupérer une liste de course par son identifiant.
     * @param id - un identifiant
     * @return - Une liste de courses.
     */
    @Override
    public ListeCourse findById(int id) {
        ListeCourse listeCourse = new ListeCourse();
        ResultSet resultat = null;
        boolean premiereLigne = true;


        try (Connection connexion = ConnectionSQLServer.seConnecter()){
            try {
                //Création de la requête SQL
                PreparedStatement preparedStatement = connexion.prepareStatement(SQLConstant.SQL_SELECT_BY_ID);

                //Insertion du paramètre id à la place du '?' dans la requête SQL
                preparedStatement.setInt(1,id);

                //Execution de la requête SQL
                resultat = preparedStatement.executeQuery();

                while (resultat.next()) {
                    if (premiereLigne) {
                        listeCourse.setId(resultat.getInt("id_liste"));
                        listeCourse.setNom(resultat.getString("nom_liste"));
                        premiereLigne = false;

                    }

                    if (resultat.getString("nom_article") != null) {
                        listeCourse .getArticles()
                                    .add(new Article(resultat.getInt("id_article"),
                                                    resultat.getString("nom_article"),
                                                    resultat.getBoolean("coche")));
                    }
                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listeCourse;
    }

    /**
     * Méthodes qui se connecte à la BDD et qui permet de lister l'ensemble des articles enregistrés.
     * @param id - un id.
     * @return - la liste d'articles en BDD.
     * @throws SQLException - Une exception de type SQLException.
     */
    @Override
    public List<ListeCourse> findAll(int id) throws SQLException {
        listeCourse = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultat = null;


        try(Connection connexion = ConnectionSQLServer.seConnecter()) {
            //Création d'un statement
            preparedStatement = connexion.prepareStatement(SQLConstant.SQL_SELECT_ALL);

            //Exécution de la requête
            resultat = preparedStatement.executeQuery();

            //Récupération des données de la requête
            while (resultat.next()) {
                listeCourse.add(new ListeCourse(resultat.getInt("id"), resultat.getString("nom")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //Fermeture des connexions
            if(resultat != null) {
                resultat.close();
            }
            if(preparedStatement != null) {
                preparedStatement.close();
            }
            if(connexion != null) {
                connexion.close();
            }
        }
        return listeCourse;
    }
}
