package fr.enrickenet.TPGestionListesCourses.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListeCourse implements Serializable
{
    //Attributs
    private int id;
    private String nom;
    private List<Article> articles;


    //Constructeurs
    public ListeCourse() { articles = new ArrayList<Article>(); }

    public ListeCourse(int id, String nom) {
        setId(id);
        setNom(nom);
    }

    public ListeCourse(int id, String nom, List<Article> articles) {
        super();
        setId(id);
        setNom(nom);
        setArticles(articles);
    }



    //Accesseurs et mutateurs

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getNom() { return nom; }

    public void setNom(String nom) { this.nom = nom; }

    public List<Article> getArticles() { return articles; }

    public void setArticles(List<Article> articles) { this.articles = articles; }
}
