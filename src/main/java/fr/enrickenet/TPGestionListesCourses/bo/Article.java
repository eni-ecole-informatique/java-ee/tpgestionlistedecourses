package fr.enrickenet.TPGestionListesCourses.bo;

import java.io.Serializable;

public class Article implements Serializable
{
    //Attributs
    private int id;
    private String nom;
    private int coche;


    //Constructueurs
    public Article() {
    }

    public Article(int id_article, String nom, boolean coche) {
        setNom(nom);
    }


    //Accesseurs et mutateurs
    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getNom() { return nom; }

    public void setNom(String nom) { this.nom = nom; }

    public int getCoche() { return coche; }

    public void setCoche(int coche) { this.coche = coche; }
}
