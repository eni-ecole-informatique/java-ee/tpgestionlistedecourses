package fr.enrickenet.TPGestionListesCourses.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.net.URL;

@WebServlet("/ServletPanier")
public class ServletPanier extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String URL_PANIER = "/WEB-INF/panier.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext()
                .getRequestDispatcher(URL_PANIER)
                .forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
