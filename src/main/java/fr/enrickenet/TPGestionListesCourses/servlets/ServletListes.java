package fr.enrickenet.TPGestionListesCourses.servlets;

import fr.enrickenet.TPGestionListesCourses.bll.ArticleManager;
import fr.enrickenet.TPGestionListesCourses.bo.Article;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/ServletListe")
public class ServletListes extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String URL_ACCUEIL = "/listes.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //On récupère les listes de courses
        ArticleManager am = new ArticleManager();
        List<Article> listeCourses = am.getArticle();

        request.setAttribute("listeCourses",listeCourses);

        this.getServletContext()
                .getRequestDispatcher(URL_ACCUEIL)
                .forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
