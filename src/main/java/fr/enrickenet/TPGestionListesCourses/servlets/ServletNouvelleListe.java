package fr.enrickenet.TPGestionListesCourses.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/ServletNouvelleListe")
public class ServletNouvelleListe extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String URL_NOUVELLELISTE = "/WEB-INF/nouvelleListe.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    this.getServletContext()
            .getRequestDispatcher(URL_NOUVELLELISTE)
            .forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
