<%--
  Created by IntelliJ IDEA.
  User: enrickenet
  Date: 01/03/2021
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Nouvelle liste</title>
    </head>
    <body>
        <%@include file="/WEB-INF/jsp/titre.jsp" %>

        <header>
            <h2>Nouvelle liste</h2>
        </header>

        <form>
            <label>Nom :</label>
            <div>
                <input type="text" placeholder="Le nom de votre liste">
            </div>
        </form>

        <div>
            <ul>
                <c:forEach var="article" items="listeArticles">
                    <li>
                        <div>
                            <a> ${ article } </a>
                            <a href="" title="Supprimer">Supprimer</a>
                        </div>
                    </li>
                </c:forEach>
                <li>
                    <label>Article :</label>
                    <input type="text" placeholder="Le nom de votre article">
                    <a href="nouvelleListe" title="Ajouter">Ajouter</a>
                </li>
            </ul>
        </div>

        <footer>
            <div>
                <a href="listes.jsp" title="retour listes">Retour</a>
            </div>
        </footer>
    </body>
</html>
